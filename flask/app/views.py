from app import app
from flask import render_template, request
from app.db_create import Initialize
import time

init_var = False

@app.route("/")
def index():
	if init_var == False:
		Initialize()
		time.sleep(3)
		init_var == True
	return render_template('index.html')

@app.route("/log", methods=['GET', 'POST'])
def log():
	if request.method == "POST":
		db_log = []
		with open('initdb.log', 'r') as output:
			db_log = output.read()
			somedata = db_log.split('\n')
		return render_template('log.html', w = somedata)
	return render_template('log.html')