import psycopg2 
import logging
import os
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


logging_level = os.environ.get('LOG_LEVEL')
database_host = os.environ.get('DB_HOST')
database_password = os.environ.get('POSTGRES_PASSWORD')

class Initialize():

	def __init__(self):

		self.set_log_level()

		with open('initdb.log', 'w'):
			pass

		### If env. vars are empty - the error occurs
		if (type(logging_level)!=str) or (type(database_host)!=str):
			self.show_message('---------Please, set env. vars LOG_LEVEL and DB_HOST----------', 'error') 
			pass
		else:
			self.create_database()
			self.initialize_database()

	### The function that can prints 
	### the message and puts it into the log file
	def show_message(self, message, level=logging_level):
		message = str(message)
		if level.upper() == 'INFO':
			logging.info(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'ERROR':
			logging.error(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'WARNING':
			logging.warrning(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'DEBUG':
			logging.debug(message)
			print(level.upper() + ' :: ' + message)
		elif level.upper() == 'CRITICAL':
			logging.critical(message)
			print(level.upper() + ' :: ' + message)

	def set_log_level(self):
		### Setting up the logging config
		if logging_level:
			if logging_level.upper() == 'DEBUG':
				logging.basicConfig(filename='initdb.log', level=logging.DEBUG,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'INFO':
				logging.basicConfig(filename='initdb.log', level=logging.INFO,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'ERROR':
				logging.basicConfig(filename='initdb.log', level=logging.ERROR,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'WARNING':
				logging.basicConfig(filename='initdb.log', level=logging.WARNING,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
			elif logging_level.upper() == 'CRITICAL':
				logging.basicConfig(filename='initdb.log', level=logging.CRITICAL,
		 			format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
		else:
			logging.basicConfig(filename='initdb.log', level=logging.ERROR,
		 		format='%(asctime)s :: %(levelname)s :: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


	def create_database(self):
		try:
			connect_str = ("user='postgres' host='{db_host}' " + \
		                      "password='{db_password}'").format(db_host=database_host, db_password=database_password)

			self.show_message('---------Connecting to the database 1----------', 'info')
			conn = psycopg2.connect(connect_str)
			conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
			cursor = conn.cursor()
			cursor.execute('''CREATE USER flask_proj;''')
			cursor.execute('''CREATE DATABASE flask_proj;''')
			cursor.execute('''GRANT ALL PRIVELEGES ON DATABASE flask_proj to flask_proj;''')
			self.show_message('---------Successfully connected to the database----------', 'info')
		except Exception as sql_exc:
			print(sql_exc)

	def initialize_database(self):	
		try:
		    connect_str = ("user='flask_proj' host='{db_host}' " + \
		                      "password=''").format(db_host=database_host)
		    self.show_message('---------Connecting to the database 2 ----------', 'info')
		    conn = psycopg2.connect(connect_str)
		    cursor = conn.cursor()
		    self.show_message('---------Creating the test table----------', 'info')
		    cursor.execute("""DROP TABLE IF EXISTS tutorials;""")
		    cursor.execute("""CREATE TABLE tutorials (name char(40));""")
		    cursor.execute("""INSERT INTO tutorials SELECT (random()::text) from generate_Series(1,5);""")
		    self.show_message('---------Inserting test values----------', 'info')
		    cursor.execute("""SELECT * from tutorials""")
		    self.show_message('---------Getting test values----------', 'info')
		    #conn.commit() # <--- makes sure the change is shown in the database
		    self.show_message('---------Commtitting the transaction----------', 'info')
		    rows = cursor.fetchall() ### <------- getting the output of the SELECT query
		    self.show_message('---------Counting rows----------', 'info')
		    try:
		    	cursor.execute("""DROP TABLE tutorials;""")
		    except Exception as e:
		    	raise e	
		    	cursor.execute("""DROP TABLE tutorials;""")
		    if len(rows) == 5:
		    	self.show_message('---------That\'s all OK----------', 'info')
		    	self.show_message('---------Deleting test table----------', 'info')
		    	self.show_message('---------Database initizalized successfully----------', 'info')
		    	#print('---------Database initialized successfully----------')
		    conn.commit()
		    cursor.close()
		    conn.close()
		except Exception as e:
			self.show_message('---------Something went wrong. Check credentials of connection to DB----------', 'error')
			print('\n')
			self.show_message(e, 'error')


